/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 * @flow
 */

import React, { Component } from 'react';
import { Text, View } from 'react-native';
import { Provider } from 'react-redux';
import { createStore } from 'redux';
import firebase from 'firebase';
import reducers from './src/reducers';

export default class App extends Component<{}> {
  componentWillMount() {
    var config = {
      apiKey: 'AIzaSyD_38AmnUsFg080-1DPKv39FtQ2WK8Kn6s',
      authDomain: 'reactnativesample-51a72.firebaseapp.com',
      databaseURL: 'https://reactnativesample-51a72.firebaseio.com',
      projectId: 'reactnativesample-51a72',
      storageBucket: 'reactnativesample-51a72.appspot.com',
      messagingSenderId: '429923539899'
    };
    firebase.initializeApp(config);
  }

  render() {
    return (
      <Provider store={ createStore(reducers) }>
        <View>
          <Text>
            Hello!
          </Text>
        </View>
      </Provider>
    );
  }
}
